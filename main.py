import argparse
import train


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', type=int, default=-1)
    parser.add_argument('--batchsize', type=int, default=128)
    parser.add_argument('--train_data', type=str)
    parser.add_argument('--n_vocab', type=int, default=100000)
    parser.add_argument('--n_units', type=int, default=256)
    parser.add_argument('--epoch', type=int, default=10)
    parser.add_argument('--max_len', type=int, default=100)
    args = parser.parse_args()
    train.train(args)


if __name__ == '__main__':
    main()
