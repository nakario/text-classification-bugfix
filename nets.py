from chainer import Chain
import chainer.functions as F
import chainer.links as L

class MLP(Chain):

    def __init__(self, n_vocab, n_mid_units, n_out):
        super(MLP, self).__init__()
        with self.init_scope():
            self.embed = L.EmbedID(n_vocab, n_mid_units)
            self.l1 = L.Linear(None, n_mid_units)
            self.l2 = L.Linear(None, n_mid_units)
            self.l3 = L.Linear(None, n_out)

    def __call__(self, x):
        e = self.embed(x)
        h1 = F.relu(self.l1(e))
        h2 = F.relu(self.l2(h1))
        return self.l3(h2)