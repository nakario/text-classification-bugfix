import collections

def create_dictionary(sentences, n):
    words = []
    dic = ['<UNK>']
    for sentence in sentences:
        words.extend(sentence.split())
    count_dict = collections.Counter(words)
    for word in count_dict.most_common(n-1):
        dic.append(word[0])
    return dic

def convert2id(sentences, word2id, max_len):
    data = []
    for sentence in sentences:
        list_id = []
        words = sentence.split()
        for i,word in enumerate(words):
            if i >= max_len:
                break
            if word in word2id.keys():
                list_id.append(word2id[word])
            else:
                list_id.append(0)
        data.append(list_id)
    return data
