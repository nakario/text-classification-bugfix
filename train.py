from collections import Counter
import chainer
from chainer import training
from chainer import iterators
import chainer.links as L
from chainer.training import extensions
import preproc
import nets

def load_data(data_path):
    labels = []
    sentences = []
    with open(data_path) as f:
        for line in f:
            label, sentence = line.strip().split(',')
            labels.append(int(label))
            sentences.append(sentence)
    return labels, sentences

def split_train_dev(data):
    boundary = len(data)*4//5
    return data[:boundary], data[boundary:]

def train(args):
    labels, sentences = load_data(args.train_data)
    dic = preproc.create_dictionary(sentences, args.n_vocab)
    dic = {w: i for i,w in enumerate(dic)}
    sentences = preproc.convert2id(sentences, dic, args.max_len)
    n_label = len(Counter(labels))
    train_labels, dev_labels = split_train_dev(labels)
    train_sentences, dev_sentences = split_train_dev(sentences)
    train_iter = iterators.SerialIterator(list(zip(train_sentences, train_labels)),args.batchsize)
    dev_iter = iterators.SerialIterator(list(zip(dev_sentences, dev_labels)),args.batchsize,False,False)

    model = L.Classifier(nets.MLP(args.n_vocab, args.n_units, n_label))
    if args.gpu >= 0:
        model.to_gpu(args.gpu)

    optimizer = chainer.optimizers.MomentumSGD()
    optimizer.setup(model)

    updater = training.updaters.StandardUpdater(train_iter, optimizer, device= args.gpu)

    trainer = training.Trainer(updater, (args.epoch, 'epoch'))
    trainer.extend(extensions.LogReport())
    trainer.extend(extensions.snapshot(filename='snapshot_epoch-{.updater.epoch}'))
    trainer.extend(extensions.snapshot_object(model.predictor, filename='model_epoch-{.updater.epoch}'))
    trainer.extend(extensions.Evaluator(dev_iter, model, device=args.gpu))
    trainer.extend(extensions.PrintReport(['epoch', 'main/loss', 'main/accuracy', 'validation/main/loss', 'validation/main/accuracy', 'elapsed_time']))
    trainer.extend(extensions.PlotReport(['main/loss', 'validation/main/loss'], x_key='epoch', file_name='loss.png'))
    trainer.extend(extensions.PlotReport(['main/accuracy', 'validation/main/accuracy'], x_key='epoch', file_name='accuracy.png'))
    trainer.extend(extensions.dump_graph('main/loss'))

    trainer.run()